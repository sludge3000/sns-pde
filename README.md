# SNS-PDE

The Stormshield Network Security Packet Dump Extractor is a python 
script to retrieve and format IP packet dumps from Stormshield IDS/IPS alarm 
log files.

Disclaimer:
This script is provided as is with absolutely no guarantees. Use at your own
risk.
This script has not been created by Stormshield and will not be supported by
Stormshield in any way.
All bugs, issues, requests and feedback regarding this script should be 
submitted to sludge3000 on GitLab.

Requirements:
snspde.py has been written for Python 3.7

Usage:
snspde.py -h

Warning:
If PktDump.hex already exists in the folder, it will be overwritten
The alarmid argument (-a) is mandatory