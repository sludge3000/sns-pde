#!/usr/bin/env python3.7
#
# Stormshield Network Security Packet Dump Extractor
# Version: 0.2 - BETA
# Created by: Luke "sludge3000" Savage
#

# Import modules
import re
import argparse
import os

from pathlib import Path


# Initialize global tables
templist1 = []
templist2 = []


# Extract all lines with specified alarm ID
def aide(aID, iFile):
    input = open(iFile)
    alarmid = 'alarmid=' + aID
    global templist2
    for line in input:
            if alarmid in line:
                templist2.append(line)


# Recurse over multiple l_alarm files
def aidmi(aID):
    p = Path(os.getcwd())
    files = list(p.glob('l_alarm*'))
    for file in files:
        aide(aID, file)


# Extract lines containing specified source IP
def filter_sourceip(sourceip):
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    source = 'src=' + sourceip
    for line in templist1:
        if source in line:
            templist2.append(line)


# Extract lines containing speciifed destination IP
def filter_destip(destip):
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    dest = 'dst=' + destip
    for line in templist1:
        if dest in line:
            templist2.append(line)


def filter_date(date):
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    fdate = 'startime="' + date
    for line in templist1:
        if fdate in line:
            templist2.append(line)


def filter_starttime(starttime):
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    stime = starttime
    for line in templist1:
        regex = re.search(
            r'startime="\d\d\d\d-\d\d-\d\d (\d\d:\d\d:\d\d)"', line
            )
        if regex.group(1) >= stime:
            templist2.append(line)


def filter_endtime(endtime):
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    etime = endtime
    for line in templist1:
        regex = re.search(
            r'startime="\d\d\d\d-\d\d-\d\d (\d\d:\d\d:\d\d)"', line
            )
        if regex.group(1) <= etime:
            templist2.append(line)


def pde():
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []
    for line in templist1:
        PktDump = re.search(r'pktdump="([0-9a-f]+)"', line)
        if PktDump is not None:
            templist2.append(PktDump.group(1))


def pf(oFile):
    output = open(oFile, 'w+')
    for line in templist2:
        new_data = ' '.join([line[i:i+2] for i in range(0, len(line), 2)])
        output.write(
            '0000 00 00 00 00 00 00 00 00 00 00 00 00 08 00 ' + new_data + '\n'
            )
    output.close()


def snspde():
    args = parser.parse_args()
    aID = args.alarmid
    iFile = args.infile
    oFile = args.outfile
    recurse = args.recurse
    sourceip = args.srcip
    destip = args.dstip
    date = args.date
    starttime = args.starttime
    endtime = args.endtime

    if recurse:
        aidmi(aID)
    else:
        aide(aID, iFile)

    if sourceip is not None:
        filter_sourceip(sourceip)

    if destip is not None:
        filter_destip(destip)

    if date is not None:
        filter_date(date)

    if starttime is not None:
        filter_starttime(starttime)

    if endtime is not None:
        filter_endtime(endtime)

    pde()
    pf(oFile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
parser.add_argument("-a", "--alarmid", help="Alarm ID")
parser.add_argument("-s", "--srcip", default=None,
                    help="Specify source IP to filter. Mandatory.")
parser.add_argument("-d", "--dstip", default=None,
                    help="Specify destination IP to filter.")
parser.add_argument("-D", "--date", default=None,
                    help="Specify the date filter in the format YYYY-MM-DD")
parser.add_argument("-sT", "--starttime", default=None,
                    help="Specify start of time filter in the format hh:mm:ss")
parser.add_argument("-eT", "--endtime", default=None,
                    help="Specify end of time filter in the format hh:mm:ss.")
parser.add_argument("-i", "--infile", help="Input file", default="l_alarm")
parser.add_argument(
    "-r", "--recurse", action='store_true',
    help="Search in all l_alarm files. Takes precedence over -i"
    )
parser.add_argument("-o", "--outfile",
                    help="Output file", default="PktDump.hex")

snspde()
